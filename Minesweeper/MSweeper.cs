﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace Minesweeper
{
    public partial class MSweeper : Form
    {
        public static string configPath = "mSweepConfig.txt";
        Random rand = new Random();
        GridSpace[,] minefield;
        public static List<Score> scoreList = new List<Score>();
        int fieldX, fieldY;
        int moves;
        int debugMode = 0;
        bool validMousePos = false, gameOver = false, enableDebugMode, uncoverAnim;
        Pen thickRed = new Pen(Color.Red, 2);
        int pfWidth, pfHeight, mines = 10, minesLeft;
        int time = 0;
        int fieldsCovered = 0;
        string[] config;
        Bitmap[] textures;

        public MSweeper()
        {
            InitializeComponent();
			

            /*string line;
            int curWidth = 9, curHeight = 9, curMines = 10;
            bool addScores = false;
            StreamReader readScoreboard = new StreamReader("mSweepScoreboard.txt");
            while ((line = readScoreboard.ReadLine()) != "//endOfFile")
            {
                if (line == "/endOfSetup")
                    addScores = false;
                if (addScores)
                    scoreList.Add(new Score(line.Split(null)[0], Convert.ToInt32(line.Split(null)[1]), curWidth, curHeight, curMines));
                if (line.Contains("["))
                {
                    curWidth = Convert.ToInt32(line.Split(null)[1]);
                    curHeight = Convert.ToInt32(line.Split(null)[2]);
                    curMines = Convert.ToInt32(line.Split(null)[3]);
                    addScores = true;
                }
            }*/

            textures = new Bitmap[13];
            textures[1] = Properties.Resources._1;
            textures[2] = Properties.Resources._2;
            textures[3] = Properties.Resources._3;
            textures[4] = Properties.Resources._4;
            textures[5] = Properties.Resources._5;
            textures[6] = Properties.Resources._6;
            textures[7] = Properties.Resources._7;
            textures[8] = Properties.Resources._8;
            textures[9] = Properties.Resources.mine;
            textures[10] = Properties.Resources.flag;
            textures[11] = Properties.Resources.flagWhite;
            textures[12] = Properties.Resources.X;
            
            start();
        }

        public void uncover(int x, int y)
        {
            bool didAThing = false;

            if (minefield[x, y].coverState != 10)
            {
                minefield[x, y].coverState += 1;
                if (minefield[x,y].coverState == 1) didAThing = true;
            }
            if (minefield[x, y].type == 0 && minefield[x, y].coverState < 2)
            {
                if (y > 1)
                    uncover(x, y - 1);
                if (y < pfHeight)
                    uncover(x, y + 1);
                if (x > 1)
                    uncover(x - 1, y);
                if (x < pfWidth)
                    uncover(x + 1, y);
                if (x > 1 && y > 1)
                    uncover(x - 1, y - 1);
                if (x < pfWidth && y > 1)
                    uncover(x + 1, y - 1);
                if (x > 1 && y < pfHeight)
                    uncover(x - 1, y + 1);
                if (x < pfWidth && y < pfHeight)
                    uncover(x + 1, y + 1);
            }
            if (uncoverAnim && didAThing)
            {
                Refresh();
            }
        }

        private void MSweeper_Paint(object sender, PaintEventArgs e)
        {
            for (int x = 1; x < pfWidth+1; x++)
            {
                int xCoord = x * 19;
                for (int y = 1; y < pfHeight + 1; y++)
                {
                    int yCoord = y * 19 + menuStrip.Height;

                    e.Graphics.FillRectangle(Brushes.White, xCoord, yCoord, 20, 20);
                    if (debugMode == 0)
                    {
                        if (!gameOver)
                        {
                            if (minefield[x, y].coverState == 0 || minefield[x, y].coverState == 10)
                            {
                                if (minefield[x, y].mouseDown == 0) e.Graphics.FillRectangle(Brushes.LightGray, xCoord, yCoord, 20, 20);
                                else if (minefield[x, y].mouseDown == 1) e.Graphics.FillRectangle(Brushes.Gray, xCoord, yCoord, 20, 20);
                                else if (minefield[x, y].mouseDown == 2) e.Graphics.FillRectangle(Brushes.Black, xCoord, yCoord, 20, 20);

                                if (minefield[x, y].coverState == 10)
                                {
                                    if (minefield[x, y].mouseDown == 2)
                                        e.Graphics.DrawImage(textures[11], xCoord, yCoord);
                                    else e.Graphics.DrawImage(textures[10], xCoord, yCoord);
                                }
                            }

                            else
                            {
                                if (minefield[x, y].type != 0 && minefield[x, y].type != 9)
                                    e.Graphics.DrawImage(textures[minefield[x, y].type], xCoord, yCoord);
                                else if (minefield[x, y].type == 9)
                                    e.Graphics.DrawImage(textures[9], xCoord, yCoord);
                            }
                        }
                        else
                        {
                            if (minefield[x, y].type == 9)
                            {
                                e.Graphics.DrawImage(textures[9], xCoord, yCoord);

                                if (minefield[x, y].coverState == 10)
                                    e.Graphics.DrawImage(textures[11], xCoord, yCoord);
                            }
                            else if (minefield[x, y].coverState == 10 && minefield[x, y].type != 9)
                            {
                                e.Graphics.DrawImage(textures[10], xCoord, yCoord);
                                e.Graphics.DrawImage(textures[12], xCoord, yCoord);
                            }
                            else if (minefield[x, y].coverState != 0)
                            {
                                if (minefield[x, y].type != 0 && minefield[x, y].type != 9)
                                    e.Graphics.DrawImage(textures[minefield[x, y].type], xCoord, yCoord);
                            }
                            else e.Graphics.FillRectangle(Brushes.LightGray, xCoord, yCoord, 20, 20);
                        }
                    }
                    else
                    {
                        if (minefield[x, y].coverState == 0 || minefield[x, y].coverState == 10)
                        {
                            if (minefield[x, y].mouseDown == 1)
                                e.Graphics.FillRectangle(Brushes.Gray, xCoord, yCoord, 20, 20);
                            if (minefield[x, y].mouseDown == 2)
                                e.Graphics.FillRectangle(Brushes.Black, xCoord, yCoord, 20, 20);
                        }
                        
                        if (minefield[x, y].type != 0 && minefield[x, y].type != 9)
                            e.Graphics.DrawImage(textures[minefield[x, y].type], xCoord, yCoord);
                        else if (minefield[x, y].type == 9)
                            e.Graphics.DrawImage(textures[9], xCoord, yCoord);

                        if (minefield[x, y].coverState == 10)
                        {
                            if (minefield[x, y].mouseDown == 2 || minefield[x, y].type == 9) e.Graphics.DrawImage(textures[11], xCoord, yCoord);
                            else e.Graphics.DrawImage(textures[10], xCoord, yCoord);
                        }
                    }
                    e.Graphics.DrawRectangle(Pens.Black, xCoord, yCoord, 19, 19);
                    //if (minefield[x, y].mouseDown == 99)
                    //    e.Graphics.DrawRectangle(Pens.Red, xCoord + 9, yCoord + 9, 2, 2);
                }
            }
        }

        private void MSweeper_MouseDown(object sender, MouseEventArgs e)
        {
            if (!gameOver)
            {
                if (e.Button == MouseButtons.Middle && enableDebugMode == true)
                {
                    if (debugMode == 0) debugMode = 1;
                    else debugMode = 0;
                }
                
                else
                {
                    fieldX = Convert.ToInt32(Math.Floor((float)e.X / 19));
                    fieldY = Convert.ToInt32(Math.Floor((float)(e.Y - menuStrip.Height) / 19));
                    if (fieldX > minefield.GetLength(0) - 2 || fieldY > minefield.GetLength(1) - 2)
                        validMousePos = false;
                    else validMousePos = true;

                    if (validMousePos && e.Button != MouseButtons.Middle) minefield[fieldX, fieldY].mouseDown = 2;
                }

                Refresh();
            }
        }

        private void MSweeper_MouseMove(object sender, MouseEventArgs e)
        {
            if (!gameOver)
            {
                fieldX = Convert.ToInt32(Math.Floor((float)e.X / 19));
                fieldY = Convert.ToInt32(Math.Floor((float)(e.Y - menuStrip.Height) / 19));
                if (fieldX > minefield.GetLength(0) - 1 || fieldY > minefield.GetLength(1) - 1)
                    validMousePos = false;
                else validMousePos = true;

                if (validMousePos)
                {
                    try
                    {
                        foreach (GridSpace s in minefield) s.mouseDown = 0;
                        if (e.Button == MouseButtons.None) minefield[fieldX, fieldY].mouseDown = 1;
                        if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) minefield[fieldX, fieldY].mouseDown = 2;
                    }
                    catch { }
                    Refresh();
                }
            }
        }

        private void MSweeper_MouseUp(object sender, MouseEventArgs e)
        {
            if (!gameOver)
            {
                if (validMousePos && fieldX >= 0 && fieldX < minefield.GetLength(0) && fieldY >= 0 && fieldY < minefield.GetLength(1))
                {
                    minefield[fieldX, fieldY].mouseDown = 0;

                    if (e.Button == MouseButtons.Left)
                    {
                        if (moves == 0)
                        {
                            if (minefield[fieldX, fieldY].type == 9)
                            {
                                int newMineX = rand.Next(1, pfWidth+1), newMineY = rand.Next(1, pfHeight+1);
                                while (minefield[newMineX, newMineY].type == 9)
                                {
                                    newMineX = rand.Next(1, pfWidth+1);
                                    newMineY = rand.Next(1, pfHeight+1);
                                }
                                minefield[newMineX, newMineY].type = 9;
                                minefield[fieldX, fieldY].type = 0;
                                putNumbers();
                            }
                        }
                        uncover(fieldX, fieldY);
                        Refresh();
                    }
                    else if (e.Button == MouseButtons.Right)
                    {
                        if (minefield[fieldX, fieldY].coverState == 0 && minesLeft > 0)
                        {
                            minefield[fieldX, fieldY].coverState = 10;
                            minesLeft--;
                        }
                        else if (minefield[fieldX, fieldY].coverState == 10)
                        {
                            minefield[fieldX, fieldY].coverState = 0;
                            minesLeft++;
                        }
                        if (minesLeft >= 100)
                            bombsText.Text = minesLeft.ToString();
                        else if (minesLeft >= 10)
                            bombsText.Text = "0" + minesLeft.ToString();
                        else
                            bombsText.Text = "00" + minesLeft.ToString();

                        Refresh();
                    }

                    if (e.Button == MouseButtons.Middle && enableDebugMode)
                        Refresh();

                    if (e.Button != MouseButtons.Middle && fieldX <= minefield.GetLength(0) - 2 && fieldY <= minefield.GetLength(1) - 2 && fieldX > 0 && fieldY > 0)
                    {
                        moves++;
                        timePassed.Start();
                    }
                }

                checkState();
            }
        }

        private void timePassed_Tick(object sender, EventArgs e)
        {
            time++;
            if (time >= 100) timeText.Text = time.ToString();
            else if (time >= 10) timeText.Text = "0" + time.ToString();
            else timeText.Text = "00" + time.ToString();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            minesLeft = mines;
            foreach (GridSpace s in minefield) if (s.coverState != 1337) s.coverState = 0;
            time = moves = fieldsCovered = 0;
            gameOver = false;
            timePassed.Stop();
            Refresh();
        }

        private void restartBtn_Click(object sender, EventArgs e)
        {
            start();
        }

        private void optionsMenu_Click(object sender, EventArgs e)
        {
            PropMenu options = new PropMenu();
            timePassed.Stop();
            options.ShowDialog(this);
            if (options.DialogResult != DialogResult.OK && moves != 0 && !gameOver) timePassed.Start();
            else start();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void putNumbers()
        {
            int proxMines = 0;
            for (int y = 1; y < pfHeight+1; y++)
            {
                for (int x = 1; x < pfWidth+1; x++)
                {
                    proxMines = 0;
                    if (minefield[x - 1, y - 1].type == 9) proxMines++;
                    if (minefield[x, y - 1].type == 9) proxMines++;
                    if (minefield[x + 1, y - 1].type == 9) proxMines++;
                    if (minefield[x - 1, y].type == 9) proxMines++;
                    if (minefield[x + 1, y].type == 9) proxMines++;
                    if (minefield[x - 1, y + 1].type == 9) proxMines++;
                    if (minefield[x, y + 1].type == 9) proxMines++;
                    if (minefield[x + 1, y + 1].type == 9) proxMines++;

                    if (minefield[x, y].type != 9 && proxMines > 0)
                        minefield[x, y].type = proxMines;
                    else if (minefield[x, y].type != 9)
                        minefield[x, y].type = 0;
                }
            }
        }

        public void start()
        {
            timePassed.Stop();
            try
            {
                config = File.ReadAllLines(configPath);
                if (File.GetAttributes(configPath) != FileAttributes.Hidden)
                    File.SetAttributes(configPath, FileAttributes.Hidden);
                pfWidth = Convert.ToInt16(config[3]);
                pfHeight = Convert.ToInt16(config[4]);
                mines = Convert.ToInt16(config[5]);
            }
            catch
            {
                StreamWriter createConfig = new StreamWriter(new FileStream(configPath, FileMode.Create, FileAccess.ReadWrite));
                createConfig.WriteLine("//playfield width");
                createConfig.WriteLine("//playfield height");
                createConfig.WriteLine("//amount of mines");
                createConfig.WriteLine("9");
                createConfig.WriteLine("9");
                createConfig.Write("10");
                createConfig.Close(); createConfig.Dispose();
                File.SetAttributes(configPath, FileAttributes.Hidden);
                config = File.ReadAllLines(configPath);
                pfWidth = Convert.ToInt16(config[3]);
                pfHeight = Convert.ToInt16(config[4]);
                mines = Convert.ToInt16(config[5]);
            }
            if (config[0].Contains("[MMouse]")) enableDebugMode = true;
            if (config[0].Contains("[UncoverAnim]")) uncoverAnim = true;
            else enableDebugMode = false;

            ClientSize = new Size((pfWidth * 19 + 1) + 38, menuStrip.Height + (pfHeight * 19 + 1) + 78);
            bottomPanel.Location = new Point((ClientSize.Width - bottomPanel.Width) / 2, ClientSize.Height - bottomPanel.Height);

            minefield = new GridSpace[pfWidth + 2, pfHeight + 2];

            for (int x = 0; x < pfWidth+2; x++)
            {
                for (int y = 0; y < pfHeight+2; y++)
                {
                    minefield[x, y] = new GridSpace();
                    if (x == 0 || x == pfWidth+1 || y == 0 || y == pfHeight+1)
                        minefield[x, y].coverState = 1337;
                }
            }

            minesLeft = mines;
            int nextMineX, nextMineY;
            while (minesLeft > 0)
            {
                nextMineX = rand.Next(1, pfWidth+1); nextMineY = rand.Next(1, pfHeight+1);
                if (minefield[nextMineX, nextMineY].type != 9)
                {
                    minefield[nextMineX, nextMineY].type = 9;
                    minesLeft--;
                }
            }
            minesLeft = mines;
            time = moves = fieldsCovered = 0;
            gameOver = false;

            putNumbers();
            if (minesLeft >= 100) bombsText.Text = minesLeft.ToString();
            else if (minesLeft >= 10) bombsText.Text = "0" + minesLeft.ToString();
            else bombsText.Text = "00" + minesLeft.ToString();

            if (time >= 100) timeText.Text = time.ToString();
            else if (time >= 10) timeText.Text = "0" + time.ToString();
            else timeText.Text = "00" + time.ToString();

            Refresh();
        }

        public void checkState()
        {
            fieldsCovered = 0;

            foreach (GridSpace s in minefield)
            {
                if (s.coverState == 0 || s.coverState == 10) fieldsCovered++;
                if (s.type == 9 && s.coverState != 0 && s.coverState != 10 && !gameOver)
                {
                    timePassed.Stop();
                    MessageBox.Show("Mine hit");
                    gameOver = true;
                    Refresh();
                }
            }

            if (!gameOver && fieldsCovered == mines)
            {
                timePassed.Stop();
                MessageBox.Show("Minefield cleared");
                gameOver = true;
                Refresh();
                /*bool onScoreboard = false;

                foreach (Score sc in scoreList)
                {
                    if (sc.width == pfWidth && sc.height == pfHeight && sc.mines == mines)
                    {
                        if (sc.time > this.time)
                            onScoreboard = true;
                        else
                            onScoreboard = false;
                    }
                }
                if (onScoreboard)
                {
                    AddScore yay = new AddScore(time, pfWidth, pfHeight, mines);
                    yay.Show();
                }*/
            }
        }
    }

    public class GridSpace
    {
        public int coverState = 0, type = 0, mouseDown = 0;
    }

    public class Score
    {
        public string name;
        public int time, width, height, mines;

        public Score(string sName, int sTime, int sWidth, int sHeight, int sMines)
        {
            name = sName; time = sTime; width = sWidth; height = sHeight; mines = sMines;
        }
    }
}