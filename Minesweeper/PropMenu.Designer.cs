﻿namespace Minesweeper
{
    partial class PropMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Save_N_Start = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.minesOpt = new System.Windows.Forms.NumericUpDown();
            this.heightOpt = new System.Windows.Forms.NumericUpDown();
            this.widthOpt = new System.Windows.Forms.NumericUpDown();
            this.nopeBtn = new System.Windows.Forms.Button();
            this.defaultsBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minesOpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightOpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthOpt)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Width (9 - 36)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Height (9 - 30)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mines (10 - 800)";
            // 
            // Save_N_Start
            // 
            this.Save_N_Start.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Save_N_Start.Location = new System.Drawing.Point(12, 115);
            this.Save_N_Start.Name = "Save_N_Start";
            this.Save_N_Start.Size = new System.Drawing.Size(118, 23);
            this.Save_N_Start.TabIndex = 3;
            this.Save_N_Start.Text = "Save values and start";
            this.Save_N_Start.Click += new System.EventHandler(this.Save_N_Start_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.minesOpt);
            this.groupBox1.Controls.Add(this.heightOpt);
            this.groupBox1.Controls.Add(this.widthOpt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 97);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Playfield properties";
            // 
            // minesOpt
            // 
            this.minesOpt.Location = new System.Drawing.Point(138, 71);
            this.minesOpt.Maximum = new decimal(new int[] {
            800,
            0,
            0,
            0});
            this.minesOpt.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.minesOpt.Name = "minesOpt";
            this.minesOpt.Size = new System.Drawing.Size(120, 20);
            this.minesOpt.TabIndex = 2;
            this.minesOpt.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // heightOpt
            // 
            this.heightOpt.Location = new System.Drawing.Point(138, 45);
            this.heightOpt.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.heightOpt.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.heightOpt.Name = "heightOpt";
            this.heightOpt.Size = new System.Drawing.Size(120, 20);
            this.heightOpt.TabIndex = 1;
            this.heightOpt.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // widthOpt
            // 
            this.widthOpt.Location = new System.Drawing.Point(138, 19);
            this.widthOpt.Maximum = new decimal(new int[] {
            36,
            0,
            0,
            0});
            this.widthOpt.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.widthOpt.Name = "widthOpt";
            this.widthOpt.Size = new System.Drawing.Size(120, 20);
            this.widthOpt.TabIndex = 0;
            this.widthOpt.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // nopeBtn
            // 
            this.nopeBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.nopeBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.nopeBtn.Location = new System.Drawing.Point(221, 115);
            this.nopeBtn.Name = "nopeBtn";
            this.nopeBtn.Size = new System.Drawing.Size(55, 23);
            this.nopeBtn.TabIndex = 5;
            this.nopeBtn.Text = "Cancel";
            this.nopeBtn.UseVisualStyleBackColor = true;
            this.nopeBtn.Click += new System.EventHandler(this.nopeBtn_Click);
            // 
            // defaultsBtn
            // 
            this.defaultsBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.defaultsBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.defaultsBtn.Location = new System.Drawing.Point(136, 115);
            this.defaultsBtn.Name = "defaultsBtn";
            this.defaultsBtn.Size = new System.Drawing.Size(79, 23);
            this.defaultsBtn.TabIndex = 4;
            this.defaultsBtn.Text = "Load defaults";
            this.defaultsBtn.UseVisualStyleBackColor = true;
            this.defaultsBtn.Click += new System.EventHandler(this.defaultsBtn_Click);
            // 
            // PropMenu
            // 
            this.AcceptButton = this.Save_N_Start;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.nopeBtn;
            this.ClientSize = new System.Drawing.Size(288, 150);
            this.Controls.Add(this.defaultsBtn);
            this.Controls.Add(this.nopeBtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Save_N_Start);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PropMenu";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Options";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minesOpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightOpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthOpt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Save_N_Start;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown minesOpt;
        private System.Windows.Forms.NumericUpDown heightOpt;
        private System.Windows.Forms.NumericUpDown widthOpt;
        private System.Windows.Forms.Button nopeBtn;
        private System.Windows.Forms.Button defaultsBtn;
    }
}