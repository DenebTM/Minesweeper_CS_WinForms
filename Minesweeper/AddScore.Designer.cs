﻿namespace Minesweeper
{
    partial class AddScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameBox = new System.Windows.Forms.TextBox();
            this.stuffLbl = new System.Windows.Forms.Label();
            this.submitBtn = new System.Windows.Forms.Button();
            this.timeLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(12, 12);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(66, 20);
            this.nameBox.TabIndex = 0;
            this.nameBox.Text = "Anonymous";
            // 
            // stuffLbl
            // 
            this.stuffLbl.AutoSize = true;
            this.stuffLbl.Location = new System.Drawing.Point(126, 16);
            this.stuffLbl.Name = "stuffLbl";
            this.stuffLbl.Size = new System.Drawing.Size(92, 13);
            this.stuffLbl.TabIndex = 1;
            this.stuffLbl.Text = "W:99 H:99 M:999";
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(224, 12);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(54, 20);
            this.submitBtn.TabIndex = 2;
            this.submitBtn.Text = "Submit";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // timeLbl
            // 
            this.timeLbl.AutoSize = true;
            this.timeLbl.Location = new System.Drawing.Point(84, 16);
            this.timeLbl.Name = "timeLbl";
            this.timeLbl.Size = new System.Drawing.Size(36, 13);
            this.timeLbl.TabIndex = 3;
            this.timeLbl.Text = "9999s";
            // 
            // AddScore
            // 
            this.AcceptButton = this.submitBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 44);
            this.ControlBox = false;
            this.Controls.Add(this.timeLbl);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.stuffLbl);
            this.Controls.Add(this.nameBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddScore";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Add your score";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label stuffLbl;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.Label timeLbl;

    }
}