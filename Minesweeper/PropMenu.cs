﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Minesweeper
{
    public partial class PropMenu : Form
    {
        string[] configFile;
        public PropMenu()
        {
            InitializeComponent();

            configFile = File.ReadAllLines(MSweeper.configPath);

            widthOpt.Value = Convert.ToInt16(configFile[3]);
            heightOpt.Value = Convert.ToInt16(configFile[4]);
            minesOpt.Value = Convert.ToInt16(configFile[5]);
        }

        private void Save_N_Start_Click(object sender, EventArgs e)
        {
            File.SetAttributes(MSweeper.configPath, FileAttributes.Normal);
            using (StreamWriter sw = new StreamWriter(MSweeper.configPath))
            {
                sw.WriteLine(configFile[0]);
                sw.WriteLine(configFile[1]);
                sw.WriteLine(configFile[2]);
                sw.WriteLine(widthOpt.Value);
                sw.WriteLine(heightOpt.Value);
                if (minesOpt.Value < widthOpt.Value * heightOpt.Value)
                    sw.Write(minesOpt.Value);
                else
                    sw.Write((widthOpt.Value * heightOpt.Value - 1).ToString());
                sw.Close(); sw.Dispose();
                Close();
                DialogResult = DialogResult.OK;
            }
            File.SetAttributes(MSweeper.configPath, FileAttributes.Hidden);
        }

        private void nopeBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void defaultsBtn_Click(object sender, EventArgs e)
        {
            widthOpt.Value = heightOpt.Value = 9;
            minesOpt.Value = 10;
            Save_N_Start_Click(sender, e);
        }
    }
}
